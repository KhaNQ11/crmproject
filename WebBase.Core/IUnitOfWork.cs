﻿using System;
using System.Threading.Tasks;
using WebBase.Core.IRepositories;

namespace WebBase.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IRoleRepository Roles { get; }
        IGroupRepository Groups { get; }
        IGroupCategoryRespository GroupCategories { get;}
        IGroupUserRespository GroupUsers { get; }
        IGroupRuleRespository GroupRules { get; }
        IGroupRuleRoleRespository GroupRuleRoles { get; }

        ICategoryRepository Categories { get; }

        int Commit();
        Task<int> CommitAsync();
        IDatabaseTransaction BeginTrainsaction();
    }
}