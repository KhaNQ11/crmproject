﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.Models.Account;

namespace WebBase.Core.Models
{
    public class GroupCategory
    {
        public int Group_Id { get; set; }
        public virtual Group_Info Group { get; set; }
        public int Cat_Id { get; set; }
        public virtual Category Category { get; set; }
    }
}
