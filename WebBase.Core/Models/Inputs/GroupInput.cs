﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core.Models.Inputs
{
    public class GroupInput
    {
        public string Group_Name { get; set; }

        public string Description { get; set; }

        public int? ParentId { get; set; }

        //public List<GroupRuleInput> Rules { get; set; }
        //public List<int> Cat_Ids { get; set; }
    }
    public class GroupRuleInput
    {
        public string Rule_Name { get; set; }

        public List<string> Roles { get; set; }
    }
}
