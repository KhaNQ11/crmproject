﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;
using System.Text;
using WebBase.Core.Abstracts;
using WebBase.Core.Models.Account;

namespace WebBase.Core.Models
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Cat_Id { get; set; }
        public string Cat_Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        [DefaultValue(null)]
        [JsonIgnore]
        public int? ParentId { get; set; }
        public virtual Category Parent { get; set; }
        public virtual ICollection<Category> Children { get; set; }
        public virtual ICollection<GroupCategory> GroupCategories { get; set; }
    }
}
