﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core.Models.Account
{
    public class GroupRuleRole
    {
        public string Role_Id { get; set; }
        public virtual Role_Info Role { get; set; }

        public int GroupRule_Id { get; set; }
        public virtual GroupRule_Info GroupRule { get; set; }
    }
}
