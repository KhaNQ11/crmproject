﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WebBase.Core.Abstracts;

namespace WebBase.Core.Models.Account
{
    public class Group_Info : Auditable
    {
        [Key]
        public int Group_Id { get; set; }
        [Required(ErrorMessage = "Group_Name: is not null.")]
        public string Group_Name { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public virtual Group_Info Parent { get; set; }
        public virtual ICollection<Group_Info> Children { get; set; }
        public virtual ICollection<GroupUser> GroupUsers { get; set; }
        public virtual ICollection<GroupRule_Info> Rules { get; set; }
        public virtual ICollection<GroupCategory> GroupCategories { get; set; }
    }
}
