﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WebBase.Core.Abstracts;

namespace WebBase.Core.Models.Account
{
    public class GroupRule_Info : Auditable
    {
        [Key]
        public int Rule_Id { get; set; }
        [Required(ErrorMessage = "Rule_Name: is not null.")]
        public string Rule_Name { get; set; }
        public int Group_Id { get; set; }
        public virtual Group_Info Group { get; set; }
        public virtual ICollection<GroupRuleRole> GroupRuleRoles { get; set; }
    }
}
