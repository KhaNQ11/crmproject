﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebBase.Core.Abstracts;
using WebBase.Core.Helpers;
using WebBase.Core.Models.Account;

namespace WebBase.Core.Models
{
    public class RoleClaim : IdentityRoleClaim<string> { }

    public class Role_Info : IdentityRole<string>, IAuditable
    {
        public Role_Info() { }

        public Role_Info(string name, string title, string description)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
            Title = title;
            Description = description;
            CreatedBy = "DevTest";
            CreatedDate = DateTime.Now.ToUnixTimestamp();
            Status = true;
        }

        [MaxLength(50)]
        public string Title { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }


        public int? CreatedDate { get; set; }

        [MaxLength(50)]
        public string CreatedBy { get; set; }

        public int? UpdatedDate { get; set; }

        [MaxLength(50)]
        public string UpdatedBy { get; set; }

        [Required]
        public bool Status { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<GroupRuleRole> GroupRuleRoles { get; set; }
    }
}