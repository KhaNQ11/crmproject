﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebBase.Core.Abstracts;
using WebBase.Core.Enums;
using WebBase.Core.Helpers;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;

namespace WebBase.Core.Models
{
    public class UserLogin : IdentityUserLogin<string> { }
    public class UserClaim : IdentityUserClaim<string> { }
    public class UserRole : IdentityUserRole<string>
    {
        public virtual User_Info User { get; set; }
        public virtual Role_Info Role { get; set; }
    }
    public class UserToken : IdentityUserToken<string> { }

    public class User_Info : IdentityUser<String>, IAuditable
    {
        public User_Info() { }
        public User_Info(
            string fullName,
            string userName,
            int birthDay,
            string email,
            string phoneNumber,
            string address,
            Gender gender,
            string identifyNumber,
            string bankNumber,
            string localPhone
        )
        {
            Id = Guid.NewGuid().ToString();
            FullName = fullName;
            UserName = userName;
            BirthDay = birthDay;
            Email = email;
            PhoneNumber = phoneNumber;
            Address = address;
            Gender = gender;
            IdentifyNumber = identifyNumber;
            BankNumber = bankNumber;
            LocalPhone = localPhone;
            CreatedBy = "DevTest";
            CreatedDate = DateTime.Now.ToUnixTimestamp();
            Status = true;
        }

        [MaxLength(128)]
        [Required]
        [Column(TypeName = "varchar(128)")]
        public override string UserName { get; set; }


        [MaxLength(256)]
        [Column(TypeName = "varchar(256)")]
        public override string Email { get; set; }

        public override bool EmailConfirmed { get; set; }

        [MaxLength(256)]
        [Column(TypeName = "varchar(256)")]
        public override string PhoneNumber { get; set; }

        public override bool PhoneNumberConfirmed { get; set; }

        [MaxLength(256)]
        [Column(TypeName = "varchar(256)")]
        public override string NormalizedEmail { get; set; }

        [MaxLength(256)]
        [Column(TypeName = "varchar(256)")]
        public override string PasswordHash { get; set; }

        public override bool LockoutEnabled { get; set; }

        public override int AccessFailedCount { get; set; }

        public override bool TwoFactorEnabled { get; set; }

        [MaxLength(256)]
        [Required]
        public string FullName { get; set; }

        [MaxLength(256)]
        public string Address { get; set; }

        public int BirthDay { get; set; }

        public Gender Gender { get; set; }

        [MaxLength(256)]
        public string Image { get; set; }

        /// <summary>
        /// CMT
        /// </summary>
        public string IdentifyNumber { get; set; }
        /// <summary>
        /// Bank Number
        /// </summary>
        public string BankNumber { get; set; }
        public string LocalPhone { get; set; }

        #region Auditable
        public int? CreatedDate { get; set; }

        [MaxLength(128)]
        [Column(TypeName = "varchar(128)")]
        public string CreatedBy { get; set; }

        public int? UpdatedDate { get; set; }

        [MaxLength(128)]
        [Column(TypeName = "varchar(128)")]
        public string UpdatedBy { get; set; }

        public bool Status { get; set; }
        #endregion

        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<GroupUser> GroupUsers { get; set; }
    }
}