﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core.Models.Account
{
    public class GroupUser
    {
        public string User_Id { get; set; }
        public virtual User_Info User { get; set; }

        public int Group_Id { get; set; }
        public virtual Group_Info Group { get; set; }
    }
}
