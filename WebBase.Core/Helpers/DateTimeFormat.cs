﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core.Helpers
{
    public static class DateTimeFormat
    {
        /// <summary>
        /// Converts a given DateTime into a Unix timestamp
        /// </summary>
        /// <param name="value">Any DateTime</param>
        /// <returns>The given DateTime in Unix timestamp format</returns>
        public static int ToUnixTimestamp(this DateTime value)
        {
            //var dateTime = new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second, DateTimeKind.Local);
            //var dateTimeOffset = new DateTimeOffset(dateTime);
            //dateTimeOffset.ToUnixTimeSeconds();

            return (int)Math.Truncate((value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
        }

        /// <summary>
        /// Gets a Unix timestamp representing the current moment
        /// </summary>
        /// <param name="ignored">Parameter ignored</param>
        /// <returns>Now expressed as a Unix timestamp</returns>
        public static int UnixTimestamp(this DateTime ignored)
        {
            return (int)Math.Truncate((DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
        }

        /// <summary>
        /// Convert a give Unix Timestamp into a DateTime.
        /// </summary>
        /// <param name="unixtime">Any Unix Timestamp</param>
        /// <returns>The given Unix timestamp in DateTime format.</returns>
        public static DateTime UnixTimestampToDateTime(this int unixtime)
        {
            return DateTimeOffset.FromUnixTimeSeconds(unixtime).DateTime.ToLocalTime();
        }
    }
}
