﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WebBase.Core.IRepositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task CreateAsync(TEntity entity);

        Task CreateRangeAsync(List<TEntity> entity);

        TEntity Create(TEntity entity);

        void Update(TEntity entity);

        Task Delete(int id);

        Task Delete(long id);

        Task Delete(string id);

        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetById(int id);

        Task<TEntity> GetById(long id);

        Task<TEntity> GetById(string id);
    }    
}