﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;

namespace WebBase.Core.IRepositories
{
    public interface IGroupRepository : IRepository<Group_Info>
    {

    }

    public interface IGroupCategoryRespository : IRepository<GroupCategory> { }

    public interface IGroupUserRespository : IRepository<GroupUser> { }
}
