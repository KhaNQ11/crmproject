﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.Models.Account;

namespace WebBase.Core.IRepositories
{
    public interface IGroupRuleRespository : IRepository<GroupRule_Info>
    {
    }

    public interface IGroupRuleRoleRespository : IRepository<GroupRuleRole> { }    
}
