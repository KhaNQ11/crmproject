﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.Models;

namespace WebBase.Core.IRepositories
{
    public interface IRoleRepository : IRepository<Role_Info>
    {
    }
}
