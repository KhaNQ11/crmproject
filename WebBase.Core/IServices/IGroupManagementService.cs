﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;
using WebBase.Core.Models.Inputs;

namespace WebBase.Core.IServices
{
    public interface IGroupManagementService
    {
        bool CheckGroupNameExisted(string groupName);

        Task<Group_Info> CreateGroupAsync(Group_Info group,List<GroupRuleInput> rules,List<int> cats);

        Task<Group_Info> UpdateGroupAsync(Group_Info group);

        Task IsEnableGroupAsync(int groupId, bool isEnable, string userUpdate);

        IEnumerable<Group_Info> GetGroups();

        IEnumerable<GroupRule_Info> GetRulesByGroupId(int groupId);

        IEnumerable<GroupCategory> GetCategoriesByGroupId(int groupId);
    }
}
