﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBase.Core.Models;

namespace WebBase.Core.IServices
{
    public interface IUserManagementService
    {
        Task<bool> IsValidUserAsync(User_Info user, string password);

        Task<User_Info> CreateUserAsync(User_Info user,string password);

        Task<User_Info> UpdateUserAsync(User_Info user);

        void CreateDefaultUser(List<User_Info> users, List<Role_Info> roles, List<Category> categories);

        Task<bool> CheckUserExistenceAsync(string userName);

        User_Info GetUserFromDbByName(string userName);

        Task<User_Info> GetUserByNameAsync(string name);

        List<User_Info> GetAllUser();

        List<Role_Info> GetRoles();
    }
}
