﻿using System;

namespace WebBase.Core.Abstracts
{
    public interface IAuditable
    {
        int? CreatedDate { get; set; }

        string CreatedBy { get; set; }

        int? UpdatedDate { get; set; }

        string UpdatedBy { get; set; }
         
        bool Status { get; set; }
    }
}