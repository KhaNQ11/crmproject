﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebBase.Core.Abstracts
{
    public abstract class Auditable : IAuditable
    {
        public int? CreatedDate { get; set; }

        [MaxLength(128)]
        [Column(TypeName = "varchar(128)")]
        public string CreatedBy { get; set; }

        public int? UpdatedDate { get; set; }

        [MaxLength(128)]
        [Column(TypeName = "varchar(128)")]
        public string UpdatedBy { get; set; }

        [DefaultValue(true)]
        public bool Status { get; set; }
    }
}