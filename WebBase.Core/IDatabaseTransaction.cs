﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core
{
    public interface IDatabaseTransaction : IDisposable
    {
        void Commit();

        void Rollback();
    }
}
