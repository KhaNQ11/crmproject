﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core.Enums
{
    public static class RoleName
    {
        //#region Application Rule
        //public const string ADMIN = "Admin";
        //public const string MANAGER = "Manager";
        //public const string UPDATE_DATA = "UpdateData";
        //public const string STAFF = "Staff";
        //#endregion

        #region User Roles
        /// <summary>
        ///  User Role
        /// </summary>
        public const string USER_INFO = "GetUserByName";
        /// <summary>
        ///  User Role
        /// </summary>
        public const string USER_All = "GetAllUser";
        /// <summary>
        ///  User Role
        /// </summary>
        public const string USER_CREATE = "CreateUser";
        /// <summary>
        ///  User Role
        /// </summary>
        public const string USER_UPDATE = "UpdateUser";
        /// <summary>
        ///  User Role
        /// </summary>
        public const string USER_DELETE = "UpdateUser";
        #endregion

        #region Group Roles
        /// <summary>
        ///  Group Role
        /// </summary>
        public const string GROUP_INFO = "GetGroupByName";
        /// <summary>
        ///  Group Role
        /// </summary>
        public const string GROUP_All = "GetAllGroup";
        /// <summary>
        ///  Group Role
        /// </summary>
        public const string GROUP_CREATE = "CreateGroup";
        /// <summary>
        ///  Group Role
        /// </summary>
        public const string GROUP_UPDATE = "UpdateGroup";
        /// <summary>
        ///  Group Role
        /// </summary>
        public const string GROUP_DELETE = "DeleteGroup";
        #endregion

        //public static List<string> RolesInPolicy(string policyName)
        //{
        //    List<string> roles = new List<string>();
        //    switch (policyName)
        //    {
        //        case PolicyName.ADMIN_PRIVILEGES:
        //            roles.Add(RoleName.ADMIN);
        //            break;
        //        case PolicyName.MANAGER_PRIVILEGES:
        //            roles.Add(RoleName.ADMIN);
        //            roles.Add(RoleName.MANAGER);
        //            break;
        //        case PolicyName.UPDATE_DATA_PRIVILEGES:
        //            roles.Add(RoleName.ADMIN);
        //            roles.Add(RoleName.MANAGER);
        //            roles.Add(RoleName.UPDATE_DATA);
        //            break;
        //        default:
        //            roles.Add(RoleName.ADMIN);
        //            roles.Add(RoleName.MANAGER);
        //            roles.Add(RoleName.UPDATE_DATA);
        //            roles.Add(RoleName.STAFF);
        //            break;
        //    }
        //    return roles;
        //}

        //public static List<string> RolesByUserRole(string userRole)
        //{
        //    List<string> roles = new List<string>();
        //    switch (userRole)
        //    {
        //        case RoleName.ADMIN:
        //            roles.Add(RoleName.ADMIN);
        //            roles.Add(RoleName.MANAGER);
        //            roles.Add(RoleName.UPDATE_DATA);
        //            roles.Add(RoleName.STAFF);
        //            break;
        //        case RoleName.MANAGER:
        //            roles.Add(RoleName.MANAGER);
        //            roles.Add(RoleName.UPDATE_DATA);
        //            roles.Add(RoleName.STAFF);
        //            break;
        //        case RoleName.UPDATE_DATA:
        //            roles.Add(RoleName.UPDATE_DATA);
        //            roles.Add(RoleName.STAFF);
        //            break;
        //        default:
        //            roles.Add(RoleName.STAFF);
        //            break;
        //    }
        //    return roles;
        //}
    }
}
