﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core.Enums
{
    public static class PolicyName
    {
        public const string ADMIN_PRIVILEGES         = "AdminPrivileges";
        public const string MANAGER_PRIVILEGES       = "ManagerPrivileges";
        public const string UPDATE_DATA_PRIVILEGES   = "UpdateDataPrivileges";
        public const string NORMAL_PRIVILEGES        = "NormalPrivileges";
    }
}
