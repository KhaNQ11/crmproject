﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBase.Core.Enums
{
    public enum Gender
    {
        Male = 1,
        Female = 2,
        Unknown = 3
    }
}
