﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;
using WebBase.Core.Models.Inputs;
using WebBase.Models.CategoryModels;
using WebBase.Models.CommonModel;
using WebBase.Models.GroupModels;
using WebBase.Models.UserModels;

namespace WebBase.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to Resource
            CreateMap<User_Info, UserResponse>();
            CreateMap<Group_Info, GroupResponse>()
                .ForMember(destination => destination.RuleCount, map=> map.MapFrom(s =>s.Rules.Count))            
                .ForMember(destination => destination.CategoryCount, map => map.MapFrom(s => s.GroupCategories.Count));
            CreateMap<GroupRuleRole, GroupRuleRolesResponse>();
            CreateMap<Role_Info, GroupRuleRolesResponse>();
            CreateMap<GroupRule_Info, GroupRuleResponse>();

            CreateMap<Role_Info, RoleOutput>();
            CreateMap<Category, CategoryResponse>();
            // Resource to Domain
            CreateMap<UserResponse, User_Info>();
            CreateMap<UserRequest, User_Info>();
            CreateMap<GroupUpdateRequest, Group_Info>();
            CreateMap<GroupCreateRequest, Group_Info>();
            CreateMap<GroupRuleRequest, GroupRule_Info>();
        }


        public static IMapper MapDataFromUserRequestToUserInfo()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserRequest, User_Info>()
                    .ForMember(destination => destination.Id, map => map.MapFrom(s => s.User_Id));
            });

            return config.CreateMapper();
        }

        public static IMapper MapDataFromUserInfoToUserResponse()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User_Info, UserResponse>()
                    .ForMember(destination => destination.User_Id, map => map.MapFrom(s => s.Id))
                    .ForMember(destination => destination.Role_Name, map => map.MapFrom(s => s.UserRoles.FirstOrDefault().Role.Name));
            });

            return config.CreateMapper();
        }
    }
}
