import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MaterialModule } from './material.module';
import { CapitalizePipe, PluralPipe, RoundPipe, TimingPipe } from './pipes';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
const components: any[] = [
  ConfirmDialogComponent,
  NavbarComponent,
  SidebarComponent,
  SnackBarComponent
];
const entryComponents: any[] = [ConfirmDialogComponent, SnackBarComponent];
const directives: any[] = [];

const pipes: any[] = [CapitalizePipe, PluralPipe, RoundPipe, TimingPipe];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    TrimValueAccessorModule
  ],
  declarations: [...components, ...directives, ...pipes],
  entryComponents: [...entryComponents],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    NgbModule,
    ...components,
    ...directives,
    ...pipes
  ],
  providers: [CookieService]
})
export class SharedModule {}
