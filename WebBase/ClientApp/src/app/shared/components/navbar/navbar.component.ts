import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/core/service/api/auth.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit() {}
  getUserName() {
    return this.authService.getUserName();
  }
}
