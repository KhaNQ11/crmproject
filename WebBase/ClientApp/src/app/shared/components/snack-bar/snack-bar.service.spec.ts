import { TestBed } from '@angular/core/testing';
import { SnackBarService, SnackBarComponent } from '.';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { TestingModule } from 'src/app/testing/testing.module';

@NgModule({
    imports: [
        TestingModule,
        MatSnackBarModule
    ],
    entryComponents: [
        SnackBarComponent
    ]
})
class DialogTestModule { }

class MockSnackBar {
    afterDismissed() {}
}

describe('SnackBarService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [MatSnackBarModule, BrowserAnimationsModule, DialogTestModule],
        providers: [SnackBarService, MatSnackBar]
    }));

    it('should be created', () => {
        const service: SnackBarService = TestBed.get(SnackBarService);
        expect(service).toBeTruthy();
    });

    it('should open SnackBar when call open() method - without messageType', () => {
        const service: SnackBarService = TestBed.get(SnackBarService);
        const snackBar: MatSnackBar = TestBed.get(MatSnackBar);
        spyOn(snackBar, 'openFromComponent').and.returnValue(new MockSnackBar());
        service.open('without messageType');
        expect(service).toBeTruthy();
        expect(snackBar).toBeTruthy();
        expect(snackBar.openFromComponent).toHaveBeenCalled();
    });

    it('should open SnackBar when call open() method - with messageType', () => {
        const service: SnackBarService = TestBed.get(SnackBarService);
        const snackBar: MatSnackBar = TestBed.get(MatSnackBar);
        spyOn(snackBar, 'openFromComponent').and.returnValue(new MockSnackBar());
        service.open('without messageType', 'success');
        expect(service).toBeTruthy();
        expect(snackBar).toBeTruthy();
        expect(snackBar.openFromComponent).toHaveBeenCalled();
    });
});
