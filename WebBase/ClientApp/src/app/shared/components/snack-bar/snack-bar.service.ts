import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { Injectable } from '@angular/core';
import { isNotEmpty } from '@app/core/utilities/helper';
import { SnackBarComponent } from './snack-bar.component';

export interface MatSnackData {
  content: string;
  type: string;
}

export interface MatSnackType {
  information: string;
  success: string;
  warning: string;
  error: string;
}

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {
  readonly type: MatSnackType = {
    information: 'information',
    success: 'success',
    warning: 'warning',
    error: 'error'
  };

  constructor(private snackBar: MatSnackBar) {}

  open(text, messageType = '', duration = 3000) {
    const matSnackData: MatSnackData = {
      content: isNotEmpty(text) ? text : '',
      type: messageType
    };

    const configSuccess: MatSnackBarConfig = {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: ['snack-bar-message', this.type.information]
    };

    if (duration) {
      configSuccess.duration = duration;
    }

    if (messageType) {
      configSuccess.panelClass = ['snack-bar-message', messageType];
    }

    return this.snackBar
      .openFromComponent(SnackBarComponent, {
        data: matSnackData,
        ...configSuccess
      })
      .afterDismissed();
  }
}
