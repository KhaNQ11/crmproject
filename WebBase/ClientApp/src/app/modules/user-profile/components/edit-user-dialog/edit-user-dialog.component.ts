import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '@app/core/service/api/user.service';
import { untilDestroyed } from '@app/core/until-destroyed';
import { UserInfo } from '@app/core/models/user.model';
import { formatDate } from '@angular/common';
import { FORMAT_DATE, LOCATION } from '@app/core/constants/common.const';
import { SnackBarService } from '@app/shared/components/snack-bar/snack-bar.service';
import { ErrorInfo } from '@app/core/models/error.model';
import { DUPLICATEUSERNAME } from '@app/core/constants/api-error.const';
import { RoleService } from '@app/core/service/api/role.service';
import { AuthService } from '@app/core/service/api/auth.service';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit, OnDestroy {
  editorForm: FormGroup;
  editorFormField = {
    createdBy: '',
    createdDate: '',
    updatedDate: '',
    updatedBy: '',
    status: true,
    user_Id: '',
    fullName: '',
    userName: '',
    birthDay: '',
    email: '',
    phoneNumber: '',
    address: '',
    gender: 1,
    identifyNumber: '',
    bankNumber: '',
    localPhone: '',
    role_Name: 'Staff',
    password: ''
  };
  title = 'Thêm mới thông tin người dùng';
  isEdit = false;
  roleList: [];
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<any>,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private roleService: RoleService,
    private authService: AuthService,
    private snackbarService: SnackBarService
  ) {
    this.editorForm = this.formBuilder.group(this.editorFormField);
  }

  ngOnInit() {
    this.roleService
      .getRoleByRule(this.authService.getUserRole())
      .subscribe(res => {
        if (res && res['isSuccess'] && res['data']) {
          this.roleList = res['data'];
        }
      });
    if (this.data.user && this.data.user.user_Id) {
      this.isEdit = true;
      this.title = 'Cập nhật thông tin người dùng';
      this.userService
        .getUserInfoByName(this.data.user.userName)
        .pipe(untilDestroyed(this))
        .subscribe(res => {
          if (res && res['isSuccess']) {
            const data = res['data'];
            data.birthDay = new Date(
              formatDate(data.birthDay * 1000, FORMAT_DATE, LOCATION)
            );
            this.editorForm = this.formBuilder.group(data);
          }
        });
    }
  }

  onSubmit() {
    this.editorForm.markAllAsTouched();
    if (this.editorForm.valid) {
      const user = this.editorForm.getRawValue() as UserInfo;
      user.birthDay = user.birthDay.getTime() / 1000;
      if (this.isEdit) {
        user.password = '1';
        this.userService
          .update(user)
          .pipe(untilDestroyed(this))
          .subscribe(this.hanlderResponse(), this.hanlderError());
      } else {
        this.userService
          .create(user)
          .pipe(untilDestroyed(this))
          .subscribe(this.hanlderResponse(), this.hanlderError());
      }
    }
  }
  private hanlderError(): (error: any) => void {
    return res => {
      let mesage = 'Có lỗi xảy ra vui lòng thử lại sau';
      if (res['status'] === 400) {
        const errorlist = res['error']['error'] as ErrorInfo[];
        if (
          errorlist &&
          errorlist.findIndex(x => x.code === DUPLICATEUSERNAME) > -1
        ) {
          mesage = 'Tên đăng nhập đã tồn tại';
        }
      }
      this.snackbarService.open(mesage, this.snackbarService.type.error);
    };
  }

  private hanlderResponse(): (value: Object) => void {
    return res => {
      if (res && res['isSuccess']) {
        this.snackbarService.open(
          'Cập nhật thành công',
          this.snackbarService.type.success
        );
        this.closeModal(true);
      }
    };
  }

  closeModal(isReload: boolean = false) {
    this.dialogRef.close({ isReload: isReload });
  }
  ngOnDestroy() {}
}
