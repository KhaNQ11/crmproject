import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../core/service/api/user.service';
import { UserInfo } from '../../core/models/user.model';
import { finalize } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { EditUserDialogComponent } from './components/edit-user-dialog/edit-user-dialog.component';
import { untilDestroyed } from '@app/core/until-destroyed';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  dataSource: UserInfo[];
  isloading = true;
  constructor(private userService: UserService, private matDialog: MatDialog) {}

  ngOnInit() {
    this.getData();
  }
  private getData() {
    this.userService
      .getAllUser()
      .pipe(
        finalize(() => (this.isloading = false)),
        untilDestroyed(this)
      )
      .subscribe(res => {
        if (res['isSuccess']) {
          this.dataSource = res['data'];
        }
      });
  }

  openDialog(item?: UserInfo) {
    const dialogRef = this.matDialog.open(EditUserDialogComponent, {
      width: '600px',
      data: { user: item }
    });
    dialogRef.afterClosed().subscribe(output => {
      if (output && output.isReload) {
        this.getData();
      }
    });
  }
  ngOnDestroy(): void {}
}
