import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';
import { SharedModule } from '@app/shared/shared.module';
import { EditUserDialogComponent } from './components/edit-user-dialog/edit-user-dialog.component';

@NgModule({
  declarations: [UserProfileComponent, EditUserDialogComponent],
  imports: [CommonModule, UserProfileRoutingModule, SharedModule],
  entryComponents:[EditUserDialogComponent]
})
export class UserProfileModule {}
