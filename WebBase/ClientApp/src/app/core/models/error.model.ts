export class ErrorInfo {
  code: string;
  description: string;
}
