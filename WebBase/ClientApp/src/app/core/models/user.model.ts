export class User {
  username: string;
  password: string;
  token: string;
}
export class UserInfo {
  createdBy: string;
  createdDate: string;
  updatedDate: string;
  updatedBy: string;
  status: boolean;
  user_Id: string;
  fullName: string;
  userName: string;
  birthDay: any;
  email: string;
  phoneNumber: string;
  address: string;
  gender: number;
  identifyNumber: string;
  bankNumber: string;
  localPhone: string;
  role_Name: string;
  password: string;
}
