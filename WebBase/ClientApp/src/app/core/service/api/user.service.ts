import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_USER_ENDPOINT } from '../../constants/api.const';
import { UserInfo } from '../../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {}
  getAllUser() {
    return this.http.get(`${API_USER_ENDPOINT}GetAllUser`).pipe(
      map(res => {
        return res as UserInfo[];
      })
    );
  }
  getUserInfoByName(userName: any) {
    const param: any = { userName: userName };
    return this.http
      .get(`${API_USER_ENDPOINT}GetUserInfoByName`, { params: param })
      .pipe(
        map(res => {
          return res as UserInfo;
        })
      );
  }
  create(user: UserInfo) {
    return this.http.post(`${API_USER_ENDPOINT}Create`, user);
  }
  update(user: UserInfo) {
    return this.http.post(`${API_USER_ENDPOINT}Update`, user);
  }
}
