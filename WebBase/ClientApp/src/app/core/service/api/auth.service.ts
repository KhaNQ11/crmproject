import { Injectable } from '@angular/core';
import { of, Observable, throwError } from 'rxjs';
import { User } from '@app/core/models/user.model';
import { HttpClient } from '@angular/common/http';
import { API_LOGIN_ENDPOINT } from '../../constants/api.const';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

interface LoginContextInterface {
    username: string;
    password: string;
    token: string;
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(private http: HttpClient, private cookieService: CookieService) { }
    login(loginContext: LoginContextInterface): Observable<any> {
        return this.http.post(`${API_LOGIN_ENDPOINT}`, loginContext).pipe(
            map(data => {
                if (data['isSuccess'] && data['data']) {
                    this.cookieService.set('jwtToken', data['data']['user_Token']);
                    this.cookieService.set(
                        'currentUser',
                        JSON.stringify(data['data']['user_Info'])
                    );
                }
                return data;
            })
        );
    }

    logout(): Observable<boolean> {
        this.cookieService.delete('jwtToken');
        this.cookieService.delete('currentUser');
        return of(false);
    }
    isLogin() {
        if (this.cookieService.check('jwtToken')) {
            return true;
        }
        return false;
    }
    getToken() {
        return this.cookieService.get('jwtToken');
    }
    getUserRole() {
        const savedCredentials = this.getUser();
        return savedCredentials['role_Name'];
    }
    getUserName() {
        const savedCredentials = this.getUser();
        return savedCredentials['userName'];
    }
    getUserInfo(): Observable<any> {
        const savedCredentials = this.getUser();
        return of(savedCredentials);
    }
    private getUser() {
        const savedCredentials = this.cookieService.get('currentUser');
        return JSON.parse(savedCredentials);
    }
}
