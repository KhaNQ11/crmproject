import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_ROLE_ENPOINT } from '@app/core/constants/api.const';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  constructor(private http: HttpClient) {}
  getRoleByRule(role: string) {
    const _param: any = { userRole: role };
    return this.http.get(`${API_ROLE_ENPOINT}`, { params: _param });
  }
}
