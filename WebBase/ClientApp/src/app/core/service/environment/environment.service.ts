import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {
  public API_URL = '';
  public clientID = '';
  public audience = '';
  public socketURL = '';
  public domain = '';
  public callbackURL = '';
  constructor() {}
}
