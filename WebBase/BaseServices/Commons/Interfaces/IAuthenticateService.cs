﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBase.Models;
using WebBase.Models.TokenModels;

namespace WebBase.BaseServices.Interfaces
{
    public interface IAuthenticateService
    {
        Task<TokenResponse> CheckTokenAuthenticatedAsync(TokenRequest request);
    }
}
