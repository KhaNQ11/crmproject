﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using WebBase.Core.Models;
using WebBase.Models.TokenModels;
using WebBase.Models.UserModels;
using WebBase.BaseServices.Interfaces;
using AutoMapper;
using WebBase.Core.IServices;

namespace WebBase.BaseServices
{
    public class TokenAuthenticationService : IAuthenticateService
    {
        private readonly IUserManagementService _userManagementService;
        private readonly TokenManagement _tokenManagement;
        private readonly IMapper _mapper;
        public TokenAuthenticationService(IUserManagementService userManagementService, IOptions<TokenManagement> tokenManagement, IMapper mapper)
        {
            _userManagementService = userManagementService;
            _tokenManagement = tokenManagement.Value;
            _mapper = mapper;
        }

        public async Task<TokenResponse> CheckTokenAuthenticatedAsync(TokenRequest request)
        {
            var user = _userManagementService.GetUserFromDbByName(request.Username);
            if (user == null)
            {
                return null;
            }

            if (!await _userManagementService.IsValidUserAsync(user, request.Password))
            {
                return null;
            }

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(nameof(User_Info.UserName),user.UserName),
            };            

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(user.UserName, "TokenAuth"),
                claims
            );
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenManagement.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            JwtSecurityTokenHandler jwtHandler = new JwtSecurityTokenHandler();
            SecurityToken aoSecurityToken = jwtHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenManagement.Issuer,
                Audience = _tokenManagement.Audience,
                SigningCredentials = credentials,
                Subject = identity,
                Expires = DateTime.Now.AddMinutes(_tokenManagement.AccessExpiration)                
            });

            TokenResponse res = new TokenResponse();
            res.User_Token = jwtHandler.WriteToken(aoSecurityToken);
            res.User_Info = _mapper.Map<User_Info, UserResponse>(user);
            return res;
        }
    }
}
