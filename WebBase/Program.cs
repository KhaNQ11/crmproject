using System;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebBase.Core.IServices;
using WebBase.Data;

namespace WebBase
{
#pragma warning disable CS1591
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var dbContext = services.GetRequiredService<WebBaseDbContext>();
                    var userManager = services.GetRequiredService<IUserManagementService>();
                    if (!dbContext.Users.Any())
                    {
                        userManager.CreateDefaultUser(WebBaseDbInitializer.DefaultUsers(), WebBaseDbInitializer.DefaultRoles(),WebBaseDbInitializer.DefaultCategories());
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred while seeding the database. \n {ex.Message}");
                }
            }
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                        .UseStartup<Startup>();
    }
#pragma warning restore CS1591
}
