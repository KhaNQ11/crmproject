﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.CategoryModels
{
    public class CategoryResponse
    {
        public int Cat_Id { get; set; }
        public string Cat_Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public int? ParentId { get; set; }
        public List<CategoryResponse> Children { get; set; }
    }
}
