﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBase.Models.UserModels;

namespace WebBase.Models.TokenModels
{
    public class TokenResponse
    {
        public string User_Token { get; set; }

        public UserResponse User_Info { get; set; }
    }
}
