﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models
{
    public class ServiceResponseModel
    {
        public ServiceResponseModel()
        {}

        public ServiceResponseModel(bool successed,object error = null,object data = null)
        {
            Succeeded = successed;
            Error = error;
            Data = data;
        }
        public bool Succeeded { get; set; } = false;
        public object Error { get; set; }
        public object Data { get; set; }
    }
}
