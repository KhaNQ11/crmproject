﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.UserModels
{
    public class UserResponse : UserBaseModel
    {
        /// <summary>
        /// User Create
        /// </summary>
        public string CreatedBy { get; set; }
        public int CreatedDate { get; set; }
        /// <summary>
        /// User Update
        /// </summary>
        public int UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool Status { get; set; }
    }
}
