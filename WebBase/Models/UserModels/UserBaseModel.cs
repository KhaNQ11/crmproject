﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebBase.Core.Enums;

namespace WebBase.Models.UserModels
{
    public class UserBaseModel
    {
        public string User_Id { get; set; }
        [MaxLength(256)]
        [Required]
        public string FullName { get; set; }
        [Required]
        public string UserName { get; set; }
        public int BirthDay { get; set; }
        [MaxLength(256)]
        [Required]
        public string Email { get; set; }
        [MaxLength(11)]
        public string PhoneNumber { get; set; }
        [MaxLength(256)]
        public string Address { get; set; }
        public Gender Gender { get; set; }

        /// <summary>
        /// CMT
        /// </summary>
        public string IdentifyNumber { get; set; }
        /// <summary>
        /// Bank Number
        /// </summary>
        public string BankNumber { get; set; }
        /// <summary>
        /// Số nội bộ
        /// </summary>
        public string LocalPhone { get; set; }

        public string Role_Name { get; set; }
    }
}
