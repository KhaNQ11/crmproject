﻿using System.ComponentModel.DataAnnotations;

namespace WebBase.Models.UserModels
{
    public class UserRequest : UserBaseModel
    {
        [Required]
        public string Password { get; set; }
    }
}
