﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models
{
    public class ResponseModel
    {
        public ResponseModel(bool isSuccess = false,string message = "",object data = null, object error = null)
        {
            IsSuccess = isSuccess;
            Message = message;
            Data = data;
            Error = error;
        }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public object Error { get; set; }
    }
}
