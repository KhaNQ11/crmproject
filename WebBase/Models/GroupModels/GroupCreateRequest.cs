﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.GroupModels
{
    public class GroupCreateRequest
    {
        [Required]
        public string Group_Name { get; set; }

        public string Description { get; set; }

        public int? ParentId { get; set; }

        public List<GroupRuleRequest> Rules { get; set; }
        public List<int> Cat_Ids { get; set; }
    }
}
