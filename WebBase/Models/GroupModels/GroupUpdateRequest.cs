﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.GroupModels
{
    public class GroupUpdateRequest
    {
        [Required]
        public int Group_Id { get; set; }

        [Required]
        public string Group_Name { get; set; }

        public string Description { get; set; }
    }
}
