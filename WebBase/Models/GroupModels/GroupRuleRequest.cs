﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.GroupModels
{
    public class GroupRuleRequest
    {
        [Required]
        public string Rule_Name { get; set; }

        public List<string> Roles { get; set; }
    }
}
