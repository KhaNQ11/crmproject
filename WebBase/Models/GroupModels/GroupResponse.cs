﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.GroupModels
{
    public class GroupResponse
    {
        public int Group_Id { get; set; }
        public string Group_Name { get; set; }
        public string Description { get; set; }
        public int RuleCount { get; set; }
        public int CategoryCount { get; set; }
        //public int? ParentId { get; set; }
        //public List<GroupResponse> Children { get; set; }
    }
}
