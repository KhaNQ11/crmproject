﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.GroupModels
{
    public class GroupRuleResponse
    {
        public int Rule_Id { get; set; }

        public string Rule_Name { get; set; }

        public IEnumerable<GroupRuleRolesResponse> Roles { get; set; }
    }

    public class GroupRuleRolesResponse
    {
        public string id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

    }
}
