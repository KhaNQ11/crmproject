﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Models.CommonModel
{
    public class RoleOutput
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
