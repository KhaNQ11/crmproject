﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebBase.Core;
using WebBase.Core.Enums;
using WebBase.Core.IServices;
using WebBase.Core.Models;
using WebBase.Data;
using WebBase.Models;
using WebBase.Models.CategoryModels;
using WebBase.Models.UserModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebBase.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    //[Authorize(Policy = PolicyName.MANAGER_PRIVILEGES)]
    [Authorize]
    public class UserManagerController : AbstractController
    {
        private readonly IUserManagementService _userService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserManagerController(IUserManagementService userService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _userService = userService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet, Route("GetUserInfoByName")]
        public IActionResult GetUserByNameAsync(string userName)
        {
            try
            {
                ResponseModel res = new ResponseModel();
                var user = _userService.GetUserByNameAsync(userName).GetAwaiter().GetResult();
                if (user != null)
                {
                    res.IsSuccess = true;
                    res.Data = _mapper.Map<UserResponse>(user);
                }
                else
                {
                    res.IsSuccess = false;
                    res.Message = "User does not exist in database.";
                }

                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseModel(false, $"Error get info user: {userName}.", null, ex.Message));
            }
        }

        [HttpGet, Route("GetAllUser")]
        public IActionResult GetAllUser()
        {
            ResponseModel res = new ResponseModel();
            var users = _userService.GetAllUser();
            var resData = _mapper.Map<List<User_Info>, List<UserResponse>>(users);
            return Ok(new ResponseModel(true, string.Empty, resData));
        }

        [HttpPost, Route("Create")]
        public IActionResult CreateNewUserAsync([FromBody] UserRequest user)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var newUser = _mapper.Map<UserRequest, User_Info>(user);
                newUser.CreatedBy = UserName();
                var resUser = _userService.CreateUserAsync(newUser, user.Password).GetAwaiter().GetResult();
                return Ok(new ResponseModel(true, string.Empty, _mapper.Map<User_Info, UserResponse>(resUser)));
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseModel(false, "Can't create new user.", null, ex.Message));
            }
        }

        [HttpPost, Route("Update")]
        public IActionResult UpdateUserAsync([FromBody] UserRequest user)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var usersUpdate = _mapper.Map<User_Info>(user);
                usersUpdate.UpdatedBy = UserName();
                var resUser = _userService.UpdateUserAsync(usersUpdate).GetAwaiter().GetResult();
                return Ok(new ResponseModel(true, string.Empty, resUser));
            }
            catch (Exception ex)
            {
                return BadRequest(new ResponseModel(false, "Can't update new user.", null, ex.Message));
            }
        }
    }
}
