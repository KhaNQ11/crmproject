﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebBase.Core;
using WebBase.Core.Enums;
using WebBase.Core.IServices;
using WebBase.Core.Models;
using WebBase.Models;
using WebBase.Models.CategoryModels;
using WebBase.Models.CommonModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebBase.Controllers
{
    [Route("api/[controller]")]
    //[Authorize(Policy = PolicyName.MANAGER_PRIVILEGES)]
    [Authorize]
    public class CommonManagerController : AbstractController
    {
        private readonly IUserManagementService _userService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommonManagerController(IUserManagementService userService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _userService = userService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet, Route("Roles")]
        public IActionResult GetRoles()
        {
            var roles = _userService.GetRoles();
            var resRoles = _mapper.Map<List<Role_Info>, List<RoleOutput>>(roles);
            return Ok(new ResponseModel(true, string.Empty, resRoles));
        }

        [HttpGet, Route("Categories")]
        public IActionResult CategoryOfUser()
        {
            var resData = _mapper.Map<List<Category>, List<CategoryResponse>>(_unitOfWork.Categories.GetMenu().ToList());
            return Ok(new ResponseModel(true, string.Empty, resData));
        }
    }
}
