﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBase.BaseServices.Interfaces;
using WebBase.Core.IServices;
using WebBase.Models;
using WebBase.Models.TokenModels;

namespace WebBase.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/TokenAuth")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticateService _authService;
        private readonly IUserManagementService _userManagementService;


        public AuthenticationController(IAuthenticateService authService, IUserManagementService userManagementService)
        {
            _authService = authService;
            _userManagementService = userManagementService;
        }

        /// <summary>
        /// User login 
        /// </summary>
        /// <remarks>
        /// Sample request :
        /// Post TokenAuth/login
        /// {
        ///     username : "User_Name",
        ///     password : "User_Password"
        /// }
        /// </remarks>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <response code="200">
        /// Return user login success.
        /// {
        ///     user_token : "token" //String,
        ///     user_info : UserResponse //info UserResponse
        /// }
        /// </response>
        /// <response code="401">Can't login.</response>  
        [AllowAnonymous]
        [HttpPost, Route("login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> RequestTokenAsync([FromBody] TokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userLogin = await _authService.CheckTokenAuthenticatedAsync(request);

            if (userLogin != null)
            {
                return Ok(new ResponseModel(true, string.Empty, userLogin));
            }

            return BadRequest(new ResponseModel(false, "Invalid Request.", null));
        }
    }
}
