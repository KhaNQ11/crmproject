﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBase.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Authorize]
    public class AbstractController : ControllerBase
    {
        protected string UserName()
        {
            var userInfo = HttpContext.User;
            return userInfo?.Claims?.SingleOrDefault(p => p.Type == "UserName")?.Value;
        }
    }
}
