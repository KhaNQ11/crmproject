﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBase.Core;
using WebBase.Core.IServices;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;
using WebBase.Core.Models.Inputs;
using WebBase.Models;
using WebBase.Models.GroupModels;
using WebBase.Models.UserModels;

namespace WebBase.Controllers
{
    [Produces("application/json")]
    [Route("api/Group")]
    [Authorize]
    public class GroupManagementController : AbstractController
    {
        private readonly IGroupManagementService _groupService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GroupManagementController(IGroupManagementService groupService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _groupService = groupService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet, Route("GetAll")]
        public IActionResult GetAllGroup()
        {
            ResponseModel res = new ResponseModel();
            var groups = _groupService.GetGroups();
            var resData = _mapper.Map<IEnumerable<Group_Info>, IEnumerable<GroupResponse>>(groups);
            return Ok(new ResponseModel(true, string.Empty, resData));
        }

        [HttpGet, Route("GetRules")]
        public IActionResult GetRuleByGroupId(int groupId)
        {
            ResponseModel res = new ResponseModel();
            var rules = _groupService.GetRulesByGroupId(groupId);
            var resRules = _mapper.Map<IEnumerable<GroupRule_Info>, IEnumerable<GroupRuleResponse>>(rules);
            foreach (var r in resRules)
            {
                var roles = rules.First(x => x.Rule_Id == r.Rule_Id).GroupRuleRoles.Select(x => x.Role).ToList();
                r.Roles = _mapper.Map<List<Role_Info>, List<GroupRuleRolesResponse>>(roles);
            }
            return Ok(new ResponseModel(true, string.Empty, resRules));
        }

        [HttpGet, Route("GetCategories")]
        public IActionResult GetCategoriesByGroupId(int groupId)
        {
            ResponseModel res = new ResponseModel();
            var categories = _groupService.GetCategoriesByGroupId(groupId);
            return Ok(new ResponseModel(true, string.Empty, categories));
        }

        [HttpPost, Route("Create")]
        public IActionResult CreateGroup([FromBody] GroupCreateRequest group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (_groupService.CheckGroupNameExisted(group.Group_Name))
            {
                return BadRequest(new ResponseModel(false, $"Group Name \"{ group.Group_Name }\" existed in Application", null, "Group Name existed!"));
            }
            var userUpdate = UserName();
            var newGroup = new Group_Info()
            {
                Group_Name = group.Group_Name,
                Description = group.Description,
                ParentId = group.ParentId,
                CreatedBy = userUpdate
            };
            var groupRules = new List<GroupRuleInput>();
            group.Rules.ForEach(r =>
            {
                groupRules.Add(new GroupRuleInput { Rule_Name = r.Rule_Name, Roles = r.Roles});
            });

            var resultGroup = _groupService.CreateGroupAsync(newGroup, groupRules,group.Cat_Ids).GetAwaiter().GetResult();
            return Ok(new ResponseModel(true, string.Empty, _mapper.Map<Group_Info, GroupResponse>(resultGroup)));
        }

        [HttpPost, Route("Update")]
        public IActionResult UpdateGroup([FromBody] GroupUpdateRequest group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var groupUpdate = _mapper.Map<Group_Info>(group);
            groupUpdate.CreatedBy = UserName();
            var resultGroup = _groupService.UpdateGroupAsync(groupUpdate).GetAwaiter().GetResult();
            return Ok(new ResponseModel(true, string.Empty, _mapper.Map<Group_Info, GroupResponse>(resultGroup)));
        }

        [HttpPost, Route("Delete")]
        public IActionResult DeleteGroup(int groupId)
        {
            _groupService.IsEnableGroupAsync(groupId,false,UserName()).GetAwaiter().GetResult();
            return Ok(new ResponseModel(true, "Delete success!"));
        }        
    }
}
