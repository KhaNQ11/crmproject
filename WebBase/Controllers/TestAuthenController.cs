﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebBase.Core.Enums;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebBase.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class TestAuthenController : Controller
    {
        [HttpGet, Route("BaseRole")]
        public IActionResult BaseRole()
        {
            return Ok($"Role: .....");
        }

        [Authorize(Roles ="Admin")]
        [HttpGet, Route("RoleBase")]
        public IActionResult RoleBase()
        {
            return Ok($"Role: .....");
        }

        [Authorize(Policy = PolicyName.ADMIN_PRIVILEGES)]
        [HttpGet, Route("AdminRole")]
        public IActionResult AdminRole()
        {
            return Ok($"Role: {PolicyName.ADMIN_PRIVILEGES}");
        }

        [Authorize(Policy = PolicyName.MANAGER_PRIVILEGES)]
        [HttpGet, Route("ManagerRole")]
        public IActionResult ManagerRole()
        {
            return Ok($"Role: {PolicyName.MANAGER_PRIVILEGES}");
        }

        [Authorize(Policy = PolicyName.UPDATE_DATA_PRIVILEGES)]
        [HttpGet, Route("UpdateRole")]
        public IActionResult UpdateRole()
        {
            return Ok($"Role: {PolicyName.UPDATE_DATA_PRIVILEGES}");
        }

        [Authorize(Policy = PolicyName.NORMAL_PRIVILEGES)]
        [HttpGet, Route("NormalRole")]
        public IActionResult NormalRole()
        {
            return Ok($"Role: {PolicyName.NORMAL_PRIVILEGES}");
        }
    }
}
