﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using WebBase.Core;
using WebBase.Core.Enums;
using WebBase.Core.Helpers;
using WebBase.Core.IServices;
using WebBase.Core.Models;

namespace WebBase.Services
{
    public class UserManagementService : IUserManagementService
    {
        private UserManager<User_Info> _userManager;
        private RoleManager<Role_Info> _roleManager;
        private IUnitOfWork _unitOfWork;
        public UserManagementService(UserManager<User_Info> userManager, RoleManager<Role_Info> roleManager, IUnitOfWork unitOfWork)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }

        public async Task<User_Info> CreateUserAsync(User_Info newUser, string password)
        {
            newUser.Id = Guid.NewGuid().ToString();
            newUser.CreatedDate = DateTime.Now.ToUnixTimestamp();
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var identity = await _userManager.CreateAsync(newUser, password);
                scope.Complete();
            }
            return newUser;
        }

        public async Task<User_Info> UpdateUserAsync(User_Info user)
        {
            var user_update = await _userManager.FindByNameAsync(user.UserName);
            user_update.Address = user.Address;
            user_update.BankNumber = user.BankNumber;
            user_update.BirthDay = user.BirthDay;
            user_update.Email = user.Email;
            user_update.FullName = user.FullName;
            user_update.Gender = user.Gender;
            user_update.IdentifyNumber = user.IdentifyNumber;
            user_update.LocalPhone = user.LocalPhone;
            user_update.UserName = user.UserName;
            user_update.PhoneNumber = user.PhoneNumber;
            user_update.UpdatedBy = user.UpdatedBy;
            user_update.UpdatedDate = DateTime.Now.ToUnixTimestamp();
            await _userManager.UpdateAsync(user_update);
            return user_update;
        }

        public async Task<bool> IsValidUserAsync(User_Info user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);
        }

        public void CreateDefaultUser(List<User_Info> users, List<Role_Info> roles, List<Category> categories)
        {
            if (!_unitOfWork.Categories.GetAll().Any())
            {
                _unitOfWork.Categories.CreateRangeAsync(categories).GetAwaiter();
            }

            if (!_roleManager.Roles.Any())
            {
                foreach (var r in roles)
                {
                    _roleManager.CreateAsync(r).GetAwaiter().GetResult();
                }
            }

            if (!_userManager.Users.Any())
            {
                foreach (var u in users)
                {
                    var user = _userManager.CreateAsync(u, "Admin@123").GetAwaiter().GetResult();
                }
            }
        }

        public async Task<bool> CheckUserExistenceAsync(string userName)
        {
            var uExist = await _userManager.FindByNameAsync(userName);
            return (uExist != null);
        }

        public User_Info GetUserFromDbByName(string userName)
        {
            return _userManager.Users.Where(x => x.UserName == userName).Include(u => u.UserRoles).ThenInclude(ur => ur.Role).FirstOrDefault();
        }

        public async Task<User_Info> GetUserByNameAsync(string userName)
        {
            return await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role)
                                                .FirstOrDefaultAsync(x => x.UserName.ToLower() == userName.Trim().ToLower());
        }

        public List<User_Info> GetAllUser()
        {
            List<User_Info> data = new List<User_Info>();
            return _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).ToList();
        }

        public List<Role_Info> GetRoles()
        {
            return _roleManager.Roles.ToList();
        }
    }
}
