﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebBase.Core;
using WebBase.Core.Helpers;
using WebBase.Core.IServices;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;
using WebBase.Core.Models.Inputs;

namespace WebBase.Services
{
    public class GroupManagementService : IGroupManagementService
    {
        private readonly IUnitOfWork _unitOfWork;

        public GroupManagementService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool CheckGroupNameExisted(string groupName)
        {
            var groups = _unitOfWork.Groups.Find(x => x.Group_Name == groupName);
            return groups.Any();
        }

        public async Task<Group_Info> CreateGroupAsync(Group_Info newGroup, List<GroupRuleInput> rules, List<int> cats)
        {
            newGroup.CreatedDate = DateTime.Now.ToUnixTimestamp();

            using (var transaction = _unitOfWork.BeginTrainsaction())
            {
                try
                {
                    await _unitOfWork.Groups.CreateAsync(newGroup);
                    await _unitOfWork.CommitAsync();
                    var newRules = new List<GroupRule_Info>();
                    rules.ForEach(r =>
                   {
                       var newRule = new GroupRule_Info
                       {
                           Rule_Name = r.Rule_Name,
                           Group_Id = newGroup.Group_Id,
                           CreatedBy = newGroup.CreatedBy,
                           CreatedDate = newGroup.CreatedDate,
                       };
                       newRules.Add(newRule);
                   });
                    await _unitOfWork.GroupRules.CreateRangeAsync(newRules);
                    await _unitOfWork.CommitAsync();

                    var groupRuleRoles = new List<GroupRuleRole>();
                    newRules.ForEach(r =>
                    {
                        var roles = rules.FirstOrDefault(x => x.Rule_Name == r.Rule_Name).Roles;
                        roles.ForEach(role =>
                        {
                            groupRuleRoles.Add(new GroupRuleRole
                            {
                                GroupRule_Id = r.Rule_Id,
                                Role_Id = role
                            });
                        });
                    });
                    await _unitOfWork.GroupRuleRoles.CreateRangeAsync(groupRuleRoles);
                    await _unitOfWork.CommitAsync();

                    var groupCategories = new List<GroupCategory>();
                    cats.ForEach(async catId =>
                    {
                        groupCategories.Add(new Core.Models.GroupCategory
                        {
                            Group_Id = newGroup.Group_Id,
                            Cat_Id = catId
                        });
                    });
                    await _unitOfWork.GroupCategories.CreateRangeAsync(groupCategories);
                    await _unitOfWork.CommitAsync();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            return newGroup;
        }

        public IEnumerable<Group_Info> GetGroups()
        {
            return _unitOfWork.Groups.GetAll();
        }

        public IEnumerable<GroupRule_Info> GetRulesByGroupId(int groupId)
        {
            var rules = _unitOfWork.GroupRules.Find(x => x.Group_Id == groupId);
            var ruleRoles = _unitOfWork.GroupRuleRoles.GetAll();
            var roles = _unitOfWork.Roles.GetAll();
            foreach (var r in rules)
            {
                var rr = ruleRoles.Where(x => x.GroupRule_Id == r.Rule_Id);
                foreach(var i in rr)
                {
                    i.Role = roles.First(x=>x.Id == i.Role_Id);
                }
                r.GroupRuleRoles = rr.ToList();
            }
            return rules;
        }

        public IEnumerable<GroupCategory> GetCategoriesByGroupId(int groupId)
        {
            return _unitOfWork.GroupCategories.Find(x => x.Group_Id == groupId);
        }

        public async Task IsEnableGroupAsync(int groupId, bool isEnable, string userUpdate)
        {
            var groupUpdate = await _unitOfWork.Groups.GetById(groupId);
            groupUpdate.UpdatedBy = userUpdate;
            groupUpdate.UpdatedDate = DateTime.Now.ToUnixTimestamp();
            groupUpdate.Status = isEnable;
            if (groupUpdate != null)
            {
                _unitOfWork.Groups.Update(groupUpdate);
            }
            await _unitOfWork.CommitAsync();
        }

        public async Task<Group_Info> UpdateGroupAsync(Group_Info group)
        {
            var updateGroup = await _unitOfWork.Groups.GetById(group.Group_Id);
            updateGroup.Group_Name = group.Group_Name;
            updateGroup.Description = group.Description;
            updateGroup.UpdatedDate = DateTime.Now.ToUnixTimestamp();
            _unitOfWork.Groups.Update(group);
            await _unitOfWork.CommitAsync();
            return group;
        }
    }
}
