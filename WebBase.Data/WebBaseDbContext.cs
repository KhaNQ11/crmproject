﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;

namespace WebBase.Data
{
    public class WebBaseDbContext : IdentityDbContext<User_Info, Role_Info, string, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>
    {
        public WebBaseDbContext(DbContextOptions<WebBaseDbContext> options) : base(options)
        { }

        public DbSet<Group_Info> Groups { get; set; }
        public DbSet<GroupRule_Info> Rules { get; set; }
        public DbSet<GroupRuleRole> GroupRuleRoles { get; set; }

        /// <summary>
        /// Menu navigation
        /// </summary>
        public DbSet<Category> Categorys { get; set; }
        public DbSet<GroupCategory> GroupCategory { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<User_Info>().Ignore(c => c.AccessFailedCount)
                                           .Ignore(c => c.EmailConfirmed)
                                           .Ignore(c => c.PhoneNumberConfirmed)
                                           .Ignore(c => c.TwoFactorEnabled)
                                           .ToTable("User");
            builder.Entity<UserLogin>().ToTable("UserLogin");
            builder.Entity<UserClaim>().ToTable("UserClaim");
            builder.Entity<UserRole>().ToTable("UserRole");
            builder.Entity<UserRole>(userRole =>
            {
                userRole.ToTable("UserRole");
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });
                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();
                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });
            builder.Entity<UserToken>().ToTable("UserToken");
            builder.Entity<Role_Info>(role =>
            {
                role.ToTable("Role");
            });
            builder.Entity<RoleClaim>().ToTable("RoleClaim");
            builder.Entity<Group_Info>().ToTable("CRM_Group")
                                            .HasMany(c => c.Children)
                                            .WithOne(child => child.Parent)
                                            .HasForeignKey(child => child.ParentId);
            builder.Entity<Category>().ToTable("CRM_Category")
                                            .HasMany(c => c.Children)
                                            .WithOne(child => child.Parent)
                                            .HasForeignKey(child => child.ParentId);

            builder.Entity<GroupRule_Info>(groupRole =>
            {
                groupRole.ToTable("CRM_GroupRules");
                groupRole.HasOne(r => r.Group).WithMany(gr =>gr.Rules).HasForeignKey(r => r.Group_Id);
            });

            builder.Entity<GroupRuleRole>(groupRuleRole =>
            {
                groupRuleRole.ToTable("CRM_GroupRuleRole");
                groupRuleRole.HasKey(grr => new { grr.GroupRule_Id, grr.Role_Id });
                groupRuleRole.HasOne(gc => gc.Role).WithMany(gc => gc.GroupRuleRoles).HasForeignKey(gc => gc.Role_Id);
                groupRuleRole.HasOne(gc => gc.GroupRule).WithMany(gc => gc.GroupRuleRoles).HasForeignKey(gc => gc.GroupRule_Id);
            });

            builder.Entity<GroupCategory>(groupCategory =>
            {
                groupCategory.ToTable("CRM_GroupCategory");
                groupCategory.HasKey(gc => new { gc.Group_Id, gc.Cat_Id });
                groupCategory.HasOne(gc => gc.Group).WithMany(gc => gc.GroupCategories).HasForeignKey(gc => gc.Group_Id);
                groupCategory.HasOne(gc => gc.Category).WithMany(gc => gc.GroupCategories).HasForeignKey(gc => gc.Cat_Id);
            });

            builder.Entity<GroupUser>(groupUser =>
            {
                groupUser.ToTable("CRM_GroupUser");
                groupUser.HasKey(gc => new { gc.Group_Id, gc.User_Id });
                groupUser.HasOne(gc => gc.Group).WithMany(gc => gc.GroupUsers).HasForeignKey(gc => gc.Group_Id);
                groupUser.HasOne(gc => gc.User).WithMany(gc => gc.GroupUsers).HasForeignKey(gc => gc.User_Id);
            });
        }
    }
}
