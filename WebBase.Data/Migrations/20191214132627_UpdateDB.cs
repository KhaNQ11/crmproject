﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBase.Data.Migrations
{
    public partial class UpdateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CRM_Category_CRM_Group_Group_InfoGroup_Id",
                table: "CRM_Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_CRM_GroupRules_GroupRule_InfoRule_Id",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Role_GroupRule_InfoRule_Id",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_CRM_Category_Group_InfoGroup_Id",
                table: "CRM_Category");

            migrationBuilder.DropColumn(
                name: "GroupRule_InfoRule_Id",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "Group_InfoGroup_Id",
                table: "CRM_Category");

            migrationBuilder.CreateTable(
                name: "CRM_GroupCategory",
                columns: table => new
                {
                    Group_Id = table.Column<int>(nullable: false),
                    Cat_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM_GroupCategory", x => new { x.Group_Id, x.Cat_Id });
                    table.ForeignKey(
                        name: "FK_CRM_GroupCategory_CRM_Category_Cat_Id",
                        column: x => x.Cat_Id,
                        principalTable: "CRM_Category",
                        principalColumn: "Cat_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM_GroupCategory_CRM_Group_Group_Id",
                        column: x => x.Group_Id,
                        principalTable: "CRM_Group",
                        principalColumn: "Group_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CRM_GroupRuleRole",
                columns: table => new
                {
                    Role_Id = table.Column<string>(nullable: false),
                    GroupRule_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM_GroupRuleRole", x => new { x.GroupRule_Id, x.Role_Id });
                    table.ForeignKey(
                        name: "FK_CRM_GroupRuleRole_CRM_GroupRules_GroupRule_Id",
                        column: x => x.GroupRule_Id,
                        principalTable: "CRM_GroupRules",
                        principalColumn: "Rule_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM_GroupRuleRole_Role_Role_Id",
                        column: x => x.Role_Id,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CRM_GroupCategory_Cat_Id",
                table: "CRM_GroupCategory",
                column: "Cat_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CRM_GroupRuleRole_Role_Id",
                table: "CRM_GroupRuleRole",
                column: "Role_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CRM_GroupCategory");

            migrationBuilder.DropTable(
                name: "CRM_GroupRuleRole");

            migrationBuilder.AddColumn<int>(
                name: "GroupRule_InfoRule_Id",
                table: "Role",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Group_InfoGroup_Id",
                table: "CRM_Category",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Role_GroupRule_InfoRule_Id",
                table: "Role",
                column: "GroupRule_InfoRule_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CRM_Category_Group_InfoGroup_Id",
                table: "CRM_Category",
                column: "Group_InfoGroup_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CRM_Category_CRM_Group_Group_InfoGroup_Id",
                table: "CRM_Category",
                column: "Group_InfoGroup_Id",
                principalTable: "CRM_Group",
                principalColumn: "Group_Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_CRM_GroupRules_GroupRule_InfoRule_Id",
                table: "Role",
                column: "GroupRule_InfoRule_Id",
                principalTable: "CRM_GroupRules",
                principalColumn: "Rule_Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
