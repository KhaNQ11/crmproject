﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBase.Data.Migrations
{
    public partial class UpdateDatabase1412 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GroupRule_InfoRule_Id",
                table: "Role",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Group_InfoGroup_Id",
                table: "CRM_Category",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CRM_GroupRules",
                columns: table => new
                {
                    Rule_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedDate = table.Column<int>(nullable: true),
                    CreatedBy = table.Column<string>(type: "varchar(128)", maxLength: 128, nullable: true),
                    UpdatedDate = table.Column<int>(nullable: true),
                    UpdatedBy = table.Column<string>(type: "varchar(128)", maxLength: 128, nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Rule_Name = table.Column<string>(nullable: false),
                    Group_Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM_GroupRules", x => x.Rule_Id);
                    table.ForeignKey(
                        name: "FK_CRM_GroupRules_CRM_Group_Group_Id",
                        column: x => x.Group_Id,
                        principalTable: "CRM_Group",
                        principalColumn: "Group_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Role_GroupRule_InfoRule_Id",
                table: "Role",
                column: "GroupRule_InfoRule_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CRM_Category_Group_InfoGroup_Id",
                table: "CRM_Category",
                column: "Group_InfoGroup_Id");

            migrationBuilder.CreateIndex(
                name: "IX_CRM_GroupRules_Group_Id",
                table: "CRM_GroupRules",
                column: "Group_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CRM_Category_CRM_Group_Group_InfoGroup_Id",
                table: "CRM_Category",
                column: "Group_InfoGroup_Id",
                principalTable: "CRM_Group",
                principalColumn: "Group_Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_CRM_GroupRules_GroupRule_InfoRule_Id",
                table: "Role",
                column: "GroupRule_InfoRule_Id",
                principalTable: "CRM_GroupRules",
                principalColumn: "Rule_Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CRM_Category_CRM_Group_Group_InfoGroup_Id",
                table: "CRM_Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_CRM_GroupRules_GroupRule_InfoRule_Id",
                table: "Role");

            migrationBuilder.DropTable(
                name: "CRM_GroupRules");

            migrationBuilder.DropIndex(
                name: "IX_Role_GroupRule_InfoRule_Id",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_CRM_Category_Group_InfoGroup_Id",
                table: "CRM_Category");

            migrationBuilder.DropColumn(
                name: "GroupRule_InfoRule_Id",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "Group_InfoGroup_Id",
                table: "CRM_Category");
        }
    }
}
