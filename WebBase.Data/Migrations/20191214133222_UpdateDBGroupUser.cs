﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBase.Data.Migrations
{
    public partial class UpdateDBGroupUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CRM_GroupUser",
                columns: table => new
                {
                    User_Id = table.Column<string>(nullable: false),
                    Group_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM_GroupUser", x => new { x.Group_Id, x.User_Id });
                    table.ForeignKey(
                        name: "FK_CRM_GroupUser_CRM_Group_Group_Id",
                        column: x => x.Group_Id,
                        principalTable: "CRM_Group",
                        principalColumn: "Group_Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM_GroupUser_User_User_Id",
                        column: x => x.User_Id,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CRM_GroupUser_User_Id",
                table: "CRM_GroupUser",
                column: "User_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CRM_GroupUser");
        }
    }
}
