﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBase.Data.Migrations
{
    public partial class GroupRules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CRM_GroupRules_CRM_Group_Group_Id",
                table: "CRM_GroupRules");

            migrationBuilder.AlterColumn<int>(
                name: "Group_Id",
                table: "CRM_GroupRules",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM_GroupRules_CRM_Group_Group_Id",
                table: "CRM_GroupRules",
                column: "Group_Id",
                principalTable: "CRM_Group",
                principalColumn: "Group_Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CRM_GroupRules_CRM_Group_Group_Id",
                table: "CRM_GroupRules");

            migrationBuilder.AlterColumn<int>(
                name: "Group_Id",
                table: "CRM_GroupRules",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_CRM_GroupRules_CRM_Group_Group_Id",
                table: "CRM_GroupRules",
                column: "Group_Id",
                principalTable: "CRM_Group",
                principalColumn: "Group_Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
