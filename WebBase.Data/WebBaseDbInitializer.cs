﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using WebBase.Core.Enums;
using WebBase.Core.Models;
using WebBase.Core.Helpers;

namespace WebBase.Data
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<WebBaseDbContext>
    {
        public WebBaseDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile(@Directory.GetCurrentDirectory() + " /../WebBase/appsettings.json")
           .Build();

            // Find a way to get different connection string 
            var connectionString = configuration.GetConnectionString("CRMConnection");

            var builder = new DbContextOptionsBuilder<WebBaseDbContext>();
            builder.UseSqlServer(connectionString);

            return new WebBaseDbContext(builder.Options);
        }
    }
    public static class WebBaseDbInitializer
    {
        public static int RandomDateTimeUnix()
        {
            var gen = new Random();
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            start.AddDays(gen.Next(range));
            return start.ToUnixTimestamp();
        }
        public static DateTime RandomDateTime()
        {
            var gen = new Random();
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            start.AddDays(gen.Next(range));
            return start;
        }

        public static List<User_Info> DefaultUsers()
        {
            List<User_Info> users = new List<User_Info> {
                new User_Info("CRM Addmin","Admin",RandomDateTimeUnix(),"AdminTest@gmail.com","0936236595","Hà Nội - Việt Nam",Gender.Male,"123265236","00231265112","0223236562"),
                new User_Info("CRM Manager","Manager1",RandomDateTimeUnix(),"Manager1Test@gmail.com","0936203232","Hà Nội - Việt Nam",Gender.Male,"123456788","00231265112","0223233333"),
                new User_Info("CRM Staff Lead","StaffUpdate",RandomDateTimeUnix(),"StaffUpdateTest@gmail.com","0936232666","Hà Nội - Việt Nam",Gender.Male,"123025456","00231265112","0223222222"),
                new User_Info("CRM Staff","Staff",RandomDateTimeUnix(),"StaffTest@gmail.com","0936232785","Hà Nội - Việt Nam",Gender.Male,"123025456","00231265112","0223221232"),
            };
            return users;
        }

        public static List<Role_Info> DefaultRoles()
        {
            List<Role_Info> roles = new List<Role_Info> {
                //User Roles
                new Role_Info(RoleName.USER_All,"Xem DS Users","Xem danh sách các user."),
                new Role_Info(RoleName.USER_INFO,"Xem TT chi tiết user","Xem thông tin chi tiết của 1 user."),
                new Role_Info(RoleName.USER_CREATE,"Thêm mới user","Tạo mới 1 user."),
                new Role_Info(RoleName.USER_UPDATE,"Sửa user","Sửa thông tin của user."),
                new Role_Info(RoleName.USER_DELETE,"Xóa user","Xóa user."),
                //Group Roles
                new Role_Info(RoleName.GROUP_All,"Xem DS Groups","Xem danh sách các group."),
                new Role_Info(RoleName.GROUP_INFO,"Xem TT chi tiết Group","Xem thông tin chi tiết của 1 group."),
                new Role_Info(RoleName.GROUP_CREATE,"Thêm mới Group","Tạo mới 1 Group."),
                new Role_Info(RoleName.GROUP_UPDATE,"Sửa Group","Sửa thông tin của Group."),
                new Role_Info(RoleName.GROUP_DELETE,"Xóa Group","Xóa Group."),

                //new Role_Info(RoleName.ADMIN,"Admin","Application management"),
                //new Role_Info(RoleName.MANAGER,"Quản lý","Quản lý nhóm, phòng ban"),
                //new Role_Info(RoleName.UPDATE_DATA,"Cập nhật dữ liệu","Có quyền cập nhật dữ liệu khách hàng"),
                //new Role_Info(RoleName.STAFF,"Nhân viên","Nhân viên")
            };
            return roles;
        }         

        /// <summary>
        /// Menu config
        /// </summary>
        /// <returns></returns>
        public static List<Category> DefaultCategories()
        {
            List<Category> cats = new List<Category>
            {
                new Category{ Cat_Id=0, Cat_Name = "Hệ thống", Description = "QL hệ thống", Url = "" },
                new Category{ Cat_Id=1,Cat_Name= "QL Nhân Viên", Description = "QL nhân viên", Url=""},
                new Category{ Cat_Id = 2, Cat_Name = "Data Khách Hàng", Description="QL data khách hàng", Url=""},
                //Danh mục Nhân viên.
                new Category{ Cat_Id=3, Cat_Name="Nhân Viên", Description="Thông tin nhân viên", Url="ThongTin-Nhan-Vien", ParentId= 1},
                new Category{ Cat_Id=4, Cat_Name="Nhóm Nhân Viên", Description="Thông tin nhóm nhân viên", Url="ThongTin-Nhom-Nhan-Vien", ParentId=1},
                new Category{ Cat_Id=5, Cat_Name="Phân Quyền", Description="Phân quyền cho nhân viên", Url="Phan-Quyen-Nhan-Vien", ParentId=1},
                //Danh mục Data KH.
                new Category{ Cat_Id=6, Cat_Name="Cập Nhật Data", Description="Update data khách hàng", Url="Update-Data-KH", ParentId=2},
                new Category{ Cat_Id=7, Cat_Name="Khách Hàng Được Giao", Description="Data khách hàng được giao khai thác", Url="Khach-Hang", ParentId=2},
                new Category{ Cat_Id=8, Cat_Name="Khách Hàng Hẹn Lịch", Description="Data khách hàng hẹn lịch", Url="Khach-Hang-Hen-Lich", ParentId=2},
            };
            return cats;
        }
    }
}
