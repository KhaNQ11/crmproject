﻿using System;
using System.Threading.Tasks;
using WebBase.Core;
using WebBase.Core.IRepositories;
using WebBase.Data.Repositories;

namespace WebBase.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WebBaseDbContext _dbContext;

        private RoleRepository _roleRepository;

        private GroupRepository _groupRepository;

        private GroupCategoryRespository _groupCategoryRespository;

        private GroupUserRespository _groupUserRespository;

        private GroupRuleRespository _groupRuleRespository;

        private GroupRuleRoleRespository _groupRuleRoleRespository;

        private CategoryRepository _categoryRepository;

        public UnitOfWork(WebBaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ICategoryRepository Categories => _categoryRepository ?? (_categoryRepository = new CategoryRepository(_dbContext));
        public IGroupRepository Groups => _groupRepository ?? (_groupRepository = new GroupRepository(_dbContext));

        public IGroupCategoryRespository GroupCategories => _groupCategoryRespository ?? (_groupCategoryRespository = new GroupCategoryRespository(_dbContext));

        public IGroupUserRespository GroupUsers => _groupUserRespository ?? (_groupUserRespository = new GroupUserRespository(_dbContext));

        public IGroupRuleRespository GroupRules => _groupRuleRespository ?? (_groupRuleRespository = new GroupRuleRespository(_dbContext));

        public IGroupRuleRoleRespository GroupRuleRoles => _groupRuleRoleRespository ?? (_groupRuleRoleRespository = new GroupRuleRoleRespository(_dbContext));

        public IRoleRepository Roles => _roleRepository ?? (_roleRepository = new RoleRepository(_dbContext));

        public IDatabaseTransaction BeginTrainsaction()
        {
            return new EntityDatabaseTransaction(_dbContext);
        }

        public int Commit()
        {
            return _dbContext.SaveChanges();
        }

        public async Task<int> CommitAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}