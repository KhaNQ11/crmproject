﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;

namespace WebBase.Data.Repositories.IRepositories
{
    public interface IGroupRepository : IRepository<Group_Info>
    {

    }
}
