﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.Models;

namespace WebBase.Data.Repositories.IRepositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        IEnumerable<Category> GetMenu();
    }
}
