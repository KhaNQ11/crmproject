﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebBase.Core.IRepositories;
using WebBase.Core.Models;
using WebBase.Core.Models.Account;

namespace WebBase.Data.Repositories
{
    public class GroupRepository : Repository<Group_Info>, IGroupRepository
    {
        protected readonly WebBaseDbContext _dbContext;
        public GroupRepository(WebBaseDbContext context) : base(context)
        {
            _dbContext = context;
        }
    }

    public class GroupCategoryRespository : Repository<GroupCategory>, IGroupCategoryRespository
    {
        protected readonly WebBaseDbContext _dbContext;
        public GroupCategoryRespository(WebBaseDbContext context) : base(context)
        {
            _dbContext = context;
        }
    }

    public class GroupUserRespository : Repository<GroupUser>, IGroupUserRespository
    {
        protected readonly WebBaseDbContext _dbContext;
        public GroupUserRespository(WebBaseDbContext context) : base(context)
        {
            _dbContext = context;
        }
    }
}
