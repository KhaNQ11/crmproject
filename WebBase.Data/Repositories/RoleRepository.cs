﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.IRepositories;
using WebBase.Core.Models;

namespace WebBase.Data.Repositories
{
    public class RoleRepository : Repository<Role_Info>, IRoleRepository
    {
        protected readonly WebBaseDbContext _dbContext;
        public RoleRepository(WebBaseDbContext context) : base(context)
        {
            _dbContext = context;
        }
    }
}
