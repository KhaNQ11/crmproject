﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebBase.Core.IRepositories;
using WebBase.Core.Models;

namespace WebBase.Data.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        protected readonly WebBaseDbContext _dbContext;
        public CategoryRepository(WebBaseDbContext context) : base(context)
        {
            _dbContext = context;
        }

        public IEnumerable<Category> GetMenu()
        {
            return _dbContext.Categorys.Include(x => x.Children).Where(x => x.ParentId == null);
        }
    }
}
