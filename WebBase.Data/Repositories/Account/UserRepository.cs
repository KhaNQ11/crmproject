﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebBase.Data.Infrastructure;
using WebBase.Data.Models;

namespace WebBase.Data.UserRepository.Account
{
    public interface IUserRepository : IGenericRepository<User_Info>
    {

    }

    public class UserRepository : GenericRepository<User_Info>, IUserRepository
    { 
        public UserRepository(IDbFactory dbFactory) : base(dbFactory)
        { 
        }         
    }
}
