﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBase.Core.IRepositories;
using WebBase.Core.Models.Account;

namespace WebBase.Data.Repositories
{
    public class GroupRuleRespository : Repository<GroupRule_Info> , IGroupRuleRespository
    {
        protected readonly WebBaseDbContext _dbContext;
        public GroupRuleRespository(WebBaseDbContext context) : base(context)
        {
            _dbContext = context;
        }
    }

    public class GroupRuleRoleRespository : Repository<GroupRuleRole>, IGroupRuleRoleRespository
    {
        protected readonly WebBaseDbContext _dbContext;
        public GroupRuleRoleRespository(WebBaseDbContext context) : base(context)
        {
            _dbContext = context;
        }
    }
}
