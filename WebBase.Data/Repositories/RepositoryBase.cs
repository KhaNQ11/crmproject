﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebBase.Core.IRepositories;

namespace WebBase.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity: class
    {
        #region Properties
        protected readonly WebBaseDbContext DbContext;
        private readonly DbSet<TEntity> _dbSet;
        #endregion

        #region Constructor
        public Repository(WebBaseDbContext dbContext)
        {
            DbContext = dbContext;
            _dbSet = DbContext.Set<TEntity>();
        }
        #endregion

        #region Implementation
        public TEntity Create(TEntity entity)
        {
            _dbSet.Add(entity);
            return entity;
        }

        public virtual async Task CreateAsync(TEntity entity)
        {
            _dbSet.AddAsync(entity);
        }

        public virtual async Task CreateRangeAsync(List<TEntity> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }

        public virtual void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual async Task Delete(int id)
        {
            TEntity aoEntity = await GetById(id);
            _dbSet.Remove(aoEntity);
        }

        public virtual async Task Delete(long id)
        {
            TEntity aoEntity = await GetById(id);
            _dbSet.Remove(aoEntity);
        }
        public virtual async Task Delete(string id)
        {
            TEntity aoEntity = await GetById(id);
            _dbSet.Remove(aoEntity);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public virtual async Task<TEntity> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual async Task<TEntity> GetById(long id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual async Task<TEntity> GetById(string id)
        {
            return await _dbSet.FindAsync(id);
        }        
        #endregion
    }
}